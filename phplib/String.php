<?php

/**
* Задача https://gitlab.com/turbo-public/backend-task-patterns/wikis/Home
* @author Shatrov Aleksej <mail@ashatrov.ru>
* @package Application
*/

	namespace Application ;

/**
* WebSocket-сервер
* @subpackage String
*/

	class String {

		/**
		* Валидация вызова метода
		* @param $name string - имя метода
		* @return boolean - метод валидирован
		*/
		public function invalidate( $name ) {
			if ( mb_substr( $name , 0 , 2 ) != 'on' ) {
				throw new \Exception( 'Вызван неизвестный метод "' . $name . '"' ) ;
			}

			return true ;
		}

		/**
		* Выполнение
		* @param $str string - строка
		* @param $method string - метод
		* @return string - результат обработки строки
		*/
		public function execute( $str , $method ) {
			$this->invalidate( $method ) ;

			return $this->$method( $str ) ;
		}

		/**
		* Удаление тегов из строки
		* @param $str string - строка
		* @return string - результат обработки строки
		*/
		public function onStripTags( $str ) {
			return strip_tags( $str ) ;
		}

		/**
		* Экранирование символов в строке
		* @param $str string - строка
		* @return string - результат обработки строки
		*/
		public function onQuotemeta( $str ) {
			return quotemeta( $str ) ;
		}

		/**
		* Удаление пробельных символов в строке, кроме символа переноса на новую строку
		* @param $str string - строка
		* @return string - результат обработки строки
		*/
		public function onRemoveSpaces( $str ) {
			return preg_replace( '{\t\r }s' , '' , $str ) ;
		}

		/**
		* Преобразование пробельных символов в строке в перенос на новую строку, кроме символа переноса на новую строку
		* @param $str string - строка
		* @return string - результат обработки строки
		*/
		public function onSpaces2EOL( $str ) {
			return preg_replace( '{\s}s' , PHP_EOL , $str ) ;
		}

		/**
		* Символы "[.,/!@#$%&*()]" должны быть удалены из строки
		* @param $str string - строка
		* @return string - результат обработки строки
		*/
		public function onClearSymbols( $str ) {
			return preg_replace( '{[.,/!@#$%&*()]}s' , '' , $str ) ;
		}

		/**
		* Найти в строке нецелые значения и привести их к целым
		* @param $str string - строка
		* @return string - результат обработки строки
		*/
		public function onToCeilValue( $str ) {
			return preg_replace_callback( '{(?:\d+)?.\d+}s' , function ( $matches ) {
				return ceil( $matches[ 0 ] ) ;
			} , $str ) ;
		}
	}