<?php
/**
* Задача https://gitlab.com/turbo-public/backend-task-patterns/wikis/Home
* @author Shatrov Aleksej <mail@ashatrov.ru>
* @package Application
*/

namespace Application ;

/**
* WebSocket-сервер
* @subpackage Application
*/

class Application extends \vakata\websocket\Server {
	/**
	* @var $str \Application\String - указатель на объект с методами преобразования строк
	*/
	protected $_str ;

	/**
	* Конструктор
	* @param $uri string - URI для подключения к WebSocket-серверу
	*/
	public function __construct( $uri ) {
		$this->_str = new \Application\String( ) ;
	}

	/**
	* Обработчик запроса клиентов
	* @param $sub - обработчик запроса клиента
	*/
	protected function onMessage( $sub ) {
		return $sub( function ( $sender , $message , $server ) use( $self ) {
			foreach ( $server->getClients( ) as $client ) {
				$server->send( intval( $client[ 'socket' ] ) , $this->_onMessage( $message ) ) ;
			}
		} ) ;
	}

	/**
	* Обработчик запроса клиента
	* @param $sub function - обработчик запроса клиента
	* @return string - результат в формате JSON
	*/
	protected function _onMessage( $message ) {
		$data = $this->_decode( $message ) ;
		$str = $data->job->text ;

		try {
			foreach ( $data->job->methods as $method ) {
				$str = $this->str->execute( $str ) ;
			}
		} catch ( \Exception $exception ) {
			return $this->_encode( array(
				'error' => $exception->getMessage( )
			) ) ;
		}

		return $this->_encode( array(
			'text' => $str
		) ) ;
	}

	/**
	* Раскодирование запроса
	* @param $str string - строка в формате JSON
	* @return object - объект JSON
	*/
	protected function _decode( $str ) {
		return json_decode( $str ) ;
	}

	/**
	* Кодирование структуры данных
	* @param $data mixed - структура данных
	* @return string - строка в формате JSON
	*/
	protected function _encode( $data ) {
		return json_encode( $data ) ;
	}
}